# TP sur PuppetServer

## Connexion 
ip machine : 145.239.43.143
username : debian
pwd : gkbdqN79T

## Autorisation d'interaction avec le repository de PuppetServer
* Installation du gestionnaire de dépendance rpm\
    `apt install rpm`
* Récupération des packages et décompression \
  On récupère la version Bullseye compatible avec Debian 11.6 (version actuelle)\
  `wget https://apt.puppet.com/puppet7-release-bullseye.deb
  sudo dpkg -i puppet7-release-bullseye.deb`
* Mise à jour des packages\
    ![img.png](img.png)
    
## Installation de PuppetServer
* Modification du Hostname et du localhost Hosts :\
  compassionate-banach.dgr.ovh \
  `/etc/hostname` \
  `/etc/hosts`
* Installation de l'exécutable\
    `apt-get install puppetserver`
* Lancement de l'exécutable\
    `systemctl start puppetserver`
* Vérification du lancement et de la version
    `systemctl status puppetserver`\
    `/opt/puppetlabs/bin/puppetserver -v`
    ![img_2.png](img_2.png)
* Diminution de la RAM allouée
    `nano /etc/default/puppetserver`
    ![img_3.png](img_3.png)

## Installation de Puppet Agent
* L'installation de PuppetServer a dû installer un PuppetAgent par défault\
    Si ce n'est pas le cas, lancer la commande :\
    `apt-get install puppet-agent`
* Lancement du service Puppet :
    ![img_4.png](img_4.png)
* Les exécutables ne se trouvent pas dans le /bin par défaut et donc pas dans le PATH.\
    Pour avoir accès aux commandes Puppet directement, Puppet met à disposition un script sh le permettant :\
    `source /etc/profile.d/puppet-agent.sh`
* Configurer le server pour ajouter la node de l'agent installé :\
    `puppet config set server puppetserver.example.com --section main`
    ![img_5.png](img_5.png)
* Ajout de la configuration dans `/etc/puppetlabs/puppet/puppet.conf`

# Préparation
Repository GitLab : https://gitlab.com/TP-Docker-CLR/tp-puppetserver \
Installation de l'extension puppet sur IntelliJ : https://plugins.jetbrains.com/plugin/7180-puppet \

* Création d'une clé SSH : \
`ssh-keygen` \
Nom du fichier de la clé : `/root/.ssh/id_rsa` \
Passphrase : null 
* Ajout de la clé publique id_rsa.pub dans le projet GitLab : \
    gitlab.com/TP-Docker-CLR -> Settings -> Repository -> Deploy keys
* Récupération de ce repository GitLab dans le dossier de production de puppetlabs: \
    `cd /etc/puppetlabs/code/environments/production` \
    `git clone git@gitlab.com:TP-Docker-CLR/tp-puppetserver.git .`
* Suppression des clés SSH du serveur puppet et restart de puppetserver : \
    `rm -Rf /etc/puppetlabs/puppet/ssl/*` \
    `systemctl restart puppetserver`

# Mise en place des services via Puppet
## Information sur Puppet
* Récupération de ce répertoire git sur le poste bureautique local.
* Ajout de l'arborescence des dossiers : \
  ├── TP PuppetServer.md\
  ├── manifests\
  │        └── site.pp\
  ├── modules\
  │       └── base\
  │               └── manifests\
  │                            └── init.pp

    Les dossiers manifests contiennent les fichiers de référencement des dépendances/modules.
    Basiquement, ils définissent la structure que puppet doit mettre en place.
    La syntaxe se base sur le langage RUST. L'extension des fichiers Puppet est `.pp`.

## Installation d'un firewall pour SSH, HTTP et HTTPS
* La forge Puppet est le site sur lequel nous pouvons retrouver les modules disponibles et préfabriqués :
  https://forge.puppet.com/modules
* Récupération des modules firewall, stdlib et concat depuis la forge. stdlib est une dépendance de firewall. Il faut donc l'installer avec.
* Une fois récupérés, décompression des paquets et renommage.\
    Puisque les paquets téléchargés sont nommés "puppet-(nomdupaquet).tar.gz)" ou "puppetlabs-(nomdupaquet).at.gz", 
    Puppet les interprète différemment. Il faut donc les renommer selon le (nomdupaquet).
* Plus qu'à les déposer dans le dossier `modules` du projet.
* Ensuite, il faut configurer les règles du FireWall dans modules/base/manifests/init.pp. \
    Exemple :\
    ```  
    firewall { '000 accept all icmp':
        proto  => 'icmp',
        action => 'accept',
    }
    ```
* Dès que les règles voulues sont configurées et poussées sur le répertoire git, il faut tirer les modifications et relancer l'agent.
    ```
    git pull;
    puppet agent -t
    ```
* Pour vérifier l'installation du service firewall :
    `iptables -nvL`

## Installation de la stack LAMP
### MYSQL
* Installation du module et de ses dépendances :
  https://forge.puppet.com/modules/puppetlabs/mysql \
    Remarque : stdlib a déjà été installée pour le firewall
* Configuration du server et création d'une base de donnée __mydb__ avec un utilisateur __rgdayan__ :
  ```
  class { 'mysql::server':
    root_password           => 'root',
    remove_default_accounts => true,
    restart                 => true,
  }

  mysql::db { 'mydb':
    user     => 'rgdayan',
    password => 'rgdayan',
    host     => 'localhost',
    grant    => ['SELECT', 'UPDATE', 'INSERT', 'DELETE'],
  }
  ```
* Modifications poussées sur le répertoire git et lancée sur puppet :
    ```
    git pull;
    puppet agent -t
    ```
* Vérification de l'installation du service et de la bd :
    ```
    mysql -u root -p
    MariaDB > SHOW DATABASES;
    ```
    ```
    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | mydb               |
    | mysql              |
    | performance_schema |
    +--------------------+
  ```
    ```
    SELECT user FROM mysql.user;
  ```
    ```
    +-------------+
    | User        |
    +-------------+
    | mariadb.sys |
    | mysql       |
    | rgdayan     |
    | root        |
    +-------------+
  ```
  
### APACHE
* Installation du module et de ses dépendances : https://forge.puppet.com/modules/puppetlabs/apache \
    https://forge.puppet.com/modules/puppetlabs/concat \
    Remarque : stdlib a déjà été installée pour le firewall
* Création d'un répertoire source pour le vhost d'apache :
    ```
    file { '/data':
      ensure  => 'directory',
      owner   => 'root',
      group   => 'root',
      mode    => '777'
    }

    file { '/data/www':
      ensure  => 'directory',
      owner   => 'root',
      group   => 'root',
      mode    => '777'
    }
  ```
* Configuration du serveur Apache par Puppet :
    ```
    class { 'apache':
      default_vhost => false,
    }
  
    apache::vhost { 'compassionate-banach.dgr.ovh':
      port    => 80,
      docroot => '/data/www',
    }
  ```
    Ici, le répertoire `/data/www` est créé en deux temps. \ 
    Puis le répertoire source du vHost d'Apache est attaché à ce dernier lors de sa création
* Pour activer php sur le serveur Apache, une modification est nécessaire dans les attributs de la class 'apache':
    ```
  class { 'apache':
    default_vhost => false,
    mpm_module       => 'prefork',
  }

  class { '::apache::mod::php': }
  ```
  Ici, nous autorisons l'activation de classes à php

### PHP
* Même chose qu'avant pour PHP 8.2 mais avec plus de dépendances nécessaires
  https://forge.puppet.com/modules/puppet/php/readme
  https://forge.puppet.com/modules/puppetlabs/apt
  https://forge.puppet.com/modules/puppetlabs/inifile
  https://forge.puppet.com/modules/puppet/zypprepo
  https://forge.puppet.com/modules/puppet/archive
* Et on ajoute à nouveau la configuration dans base/init.pp :
  ```
    class { '::php::globals':
      php_version => '8.2',
      config_root => '/etc/php/8.2',
    }->
    class { '::php':
      ensure       => latest,
      manage_repos => true,
      fpm          => true,
      dev          => true,
      composer     => true,
      pear         => true,
      phpunit      => false,
    }
  ```
  
## Utilisation d'un template
Nous allons créer un template pour la page d'accueil 'index.php' du serveur apache.
Le but est de générer un fichier index.php récupérer des données du script init.pp.

* Dans un premier temps, on va créer le dossier modules/base/templates et y ajouter un 
  fichier index.php.epp. EPP est l'extension des templates dans la syntaxe Embedded Puppet.
* Nous ajoutons un contenu définissant un paramètre et on l'utilise juste après. 
  En passant, on peut aussi mettre un peu de code PHP pour vérifier que le serveur Apache l'interprète correctement.
    ```
    <%- | String $hostname | -%>
    Nom du VHost : <%= $hostname -%>
    
    </br>
    <?php
        echo 'Test du fonctionnement de php : Parfait !'
    ?>
  ```
* Dans init.php, on peut créer un fichier index.php dans la racine du vhost d'Apache, à savoir '/data/www/'.
  On lui assignera le contenu du template créé ci-dessus :
    ```
  file { '/data/www/index.php':
      ensure  => 'file',
      owner   => 'root',
      mode    => '644',
      content => epp('base/index.php.epp', 
        { 'hostname' => 'une variable passée en argument'}),
  }
  ```
* On synchronise le repos git, récupère les modifications sur le serveur et on relance l'agent.
