class base {
  Firewall {
    require => undef,
  }

  class { 'firewall': }

  # Default firewall rules
  firewall { '000 accept all icmp':
    proto  => 'icmp',
    action => 'accept',
  }
  -> firewall { '001 accept all to lo interface':
    proto   => 'all',
    iniface => 'lo',
    action  => 'accept',
  }
  -> firewall { '002 reject local traffic not on loopback interface':
    iniface     => '! lo',
    proto       => 'all',
    destination => '127.0.0.1/8',
    action      => 'reject',
  }
  -> firewall { '003 accept related established rules':
    proto  => 'all',
    state  => ['RELATED', 'ESTABLISHED'],
    action => 'accept',
  }
  -> firewall { '100 allow http and https access':
    dport  => [80, 443],
    proto  => 'tcp',
    action => 'accept',
  }
  -> firewall { '101 allow ssh access':
    dport  => 22,
    proto  => 'tcp',
    action => 'accept',
  }

  firewall { '999 drop all':
    proto  => 'all',
    action => 'drop',
    before => undef,
  }

  class { 'mysql::server':
    root_password           => 'root',
    remove_default_accounts => true,
    restart                 => true,
  }

  mysql::db { 'mydb':
    user     => 'rgdayan',
    password => 'rgdayan',
    host     => 'localhost',
    grant    => ['SELECT', 'UPDATE', 'INSERT', 'DELETE'],
  }

  class { '::php::globals':
    php_version => '8.2',
    config_root => '/etc/php/8.2',
  }->
  class { '::php':
    ensure       => latest,
    manage_repos => true,
    fpm          => true,
    dev          => true,
    composer     => true,
    pear         => true,
    phpunit      => false,
  }

  class { 'apache':
    default_vhost => false,
    mpm_module       => 'prefork',
  }

  class { '::apache::mod::php': }

  ::apache::vhost { 'compassionate-banach.dgr.ovh':
    docroot         => '/data/www',
    port            => 80,
  }

  file { '/data':
    ensure  => 'directory',
    owner   => 'root',
    group   => 'root',
    mode    => '644'
  }

  file { '/data/www':
    ensure  => 'directory',
    owner   => 'root',
    group   => 'root',
    mode    => '644'
  }

  file { '/data/www/index.php':
    ensure  => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '644',
    content => epp('base/index.php.epp',
      { 'hostname' => 'une variable passée en argument'}),
  }
}